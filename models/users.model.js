const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const usersSchema = mongoose.Schema({
    name: {type: String, default: ''},
    email: {type: String, unique: true, required: true , lowercase: true},
    password: {type: String, default: ''},
    friendsIds: [{ type: String, default: ''}],
}, {
        timestamps: true
    })

    usersSchema.methods.isValid = function(hashedpassword){
        return  bcrypt.compareSync(hashedpassword, this.password);
    }
  module.exports = mongoose.model('usersSchema', usersSchema);