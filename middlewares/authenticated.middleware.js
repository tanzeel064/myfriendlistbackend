const jwt = require('jsonwebtoken');
const userSchema = require('../models/users.model')

function auth(req, res, next) {
  
    let token = req.headers['x-access-token'];
    if (!token) {
      return res.status(403).send({ auth: false, message: 'No token provided.' });
    }
    jwt.verify(token, 'a1a2a3a4a5', (err, decoded) => {
      if (err) {
        return res.status(403).send({ auth: false, message: 'Failed to authenticate token.' });
      }
      req.userId = decoded.id;
      // console.log(req.userId)
      userSchema.findById(req.userId)
        .then(user => {
          if (!user) {
            return res.status(403).send({ auth: false, message: 'No authenticated user found' });
          }
          else {
            req.userData = user;
            req.userData.entity = 'Bidieye';
          }
          next();
        })
        .catch(err => {
          logger.error(err)
          return res.status(500).send({ auth: false, message: 'Error in fetching user' + err });
        })
    });
  
}

module.exports = auth;
