const userSchems = require('../models/users.model');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const error = require('../error')
const loadash = require('lodash');
const passport = require('passport');
const nodemailer = require("nodemailer");

const mailTransport = nodemailer.createTransport({
    service: "gmail",
    host: "stmp.gmail.com",
    port: 587,
    secure: false,
    auth: {
        user: "your-gmail-address",
        pass: "your-gmail-password"
    },
    authMethod: 'NTLM',
    tls: {
        rejectUnauthorized: false
    },
    debug: true
});

exports.register = (req, res) => {
    if (req.body.password != undefined) {
        let hashedPassword = bcrypt.hashSync(req.body.password, 8);
        const userSchema = new userSchems({
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword,
            friendsIds: req.body.friendsIds
        })
        userSchema.save()
            .then(user => {
                if (!user || user == null) {
                    return res.status(200).send({
                        response: 'User not added.',
                        data: {}
                    });
                }
                sendEmail(user.email);
                res.status(200).send({ data: user, message: "User created Successfully" });
            })
            .catch(err => {
                let errorObject = error.getErrorMessage(err)
                res.status(errorObject.code).send({ message: errorObject.message })
            })
    } else {
        res.status(500).send('Empty password')
    }
}

exports.login = (req, res, next) => {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return res.status(501).send(err);
        }
        if (!user) {
            return res.status(501).send(info);
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(501).json(error.getErrorMessage(err));
            }
            let token = jwt.sign({ id: user._id }, 'a1a2a3a4a5', {
                expiresIn: "7 days"
            });
            user = user.toObject();
            return res.status(200).send({ user: user, token: token, message: 'Login Success' });
        });
    })(req, res, next);
}

exports.getAllUsers = (req, res) => {
    userSchems.find()
        .then(users => {
            if (!users || users == null) {
                res.status(200).send({
                    message: "Record not found",
                    data: []
                })
            }
            res.status(200).send({
                message: "Record fetched successfully",
                data: users
            })
        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}
exports.addFriend = (req, res) => {
    let data = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        friendsIds: req.body.friendsIds
    }
    userSchems.findByIdAndUpdate(req.params.id, { $set: data }, { new: true })
        .then(user => {
            if (!user || user == null) {
                res.status(200).send({
                    message: "Record not found",
                    data: {}
                })
            }
            res.status(200).send({
                message: "Record updated successfully",
                data: user
            })
        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}

exports.getAllFriends = (req, res) => {
    let friends = [];
    userSchems.findById(req.params.id)
        .then(users => {
            if (!users || users == null) {
                res.status(200).send({
                    message: "Record not found",
                    data: []
                })
            }
            console.log(users.friendsIds)
            if (users.friendsIds.length > 1) {
                friends = [];
                console.log(users.friendsIds.length);
                users.friendsIds.forEach((id, i) => {
                    console.log(id);
                    if (id !== "") {
                        userSchems.findById(id).then(user => {
                            friends.push(user);
                            if (users.friendsIds.length == (i + 1)) {
                                res.status(200).send({
                                    message: "Record fetched successfully",
                                    data: friends
                                })
                            }
                        })
                    }
                });
            } else {
                res.status(200).send({
                    message: "No Friend Found!",
                    data: []
                })
            }
        })
        .catch(err => {
            let errorObject = error.getErrorMessage(err)
            res.status(errorObject.code).send({ message: errorObject.message })
        })
}

const sendEmail = (receiver) => {
    console.log(receiver)
    const mailOptions = {
        to: receiver,
        from: `"Friend List" <myFriendList@support.com>`,
        subject: 'Welcome to Friend List',
        text:"Thank you !"
    };

    mailTransport.sendMail(mailOptions).then(() => {

        console.log("Email sent successfully!")
    }).catch((err ) => {
        console.error(err.message);
    });
}