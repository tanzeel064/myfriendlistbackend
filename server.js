
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const appConfig = require('./config/app');
const mongoose = require('mongoose');
const fs = require('fs');
const http = require('http');
const https = require('https');
var session = require('express-session');
require('./config/passport');
const app = express();
const cors = require('cors');
var path = require("path");

app.use(cors());


app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

appConfig(app);
app.use(passport.initialize())
app.use(passport.session())


mongoose.Promise = global.Promise;   
mongoose.set('useCreateIndex', true)
mongoose.connect('mongodb://localhost:27017/friend-list',{ useNewUrlParser: true })
.then(() => {
  console.log("Successfully connected to the database");
})
.catch(err => {
  console.log('Could not connect to the database. Exiting now...');
    process.exit();  // to close app Completely 
})
require('./models/users.model')
app.get('/', (req, res) => {
  res.json({"message": "Welcome to Friend List"});
});
// app.get('/api-docs.json', (req, res) => {
//   res.setHeader('Content-Type', 'application/json');
//   res.send(swaggerSpec.swaggerSpec);
// });
const MongoStore = require('connect-mongo')(session);
app.use(session({
  name:'id',
  resave:false,
  saveUninitialized:false,
  secret:'a1a2a3a4a5',
  cookie:{
    maxAge:36000000,
    httpOnly:false,
    secure:false
  },
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}));


require('./routes/users.route')(app)

var httpServer = http.createServer(app);

httpServer.listen(3000);