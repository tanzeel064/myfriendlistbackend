const auth = require('../middlewares/authenticated.middleware');
module.exports = (app) =>{
const passport = require('passport');
const userController = require('../controllers/users.controller')
    app.post('/api/register',userController.register);
    app.post('/api/login', userController.login);
    app.put('/api/addFriend/:id', auth, userController.addFriend);
    app.get('/api/getAllUsers', auth, userController.getAllUsers);
    app.get('/api/getAllFriends/:id', auth, userController.getAllFriends);
}